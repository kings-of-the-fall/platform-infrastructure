variable "name" {
  description = "the name of your stack, e.g. hellomicro"
}

variable "environment" {
  description = "the name of your environment, e.g. dev/staging/dev/staging/prod"
}
