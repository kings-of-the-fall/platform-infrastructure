# AWS Infrastructure with Terraform

## Overview
This Terraform project provisions a complete AWS infrastructure for running containerized applications using **Amazon ECS (Fargate), an Application Load Balancer (ALB), and an Elastic Container Registry (ECR)**. The setup ensures high availability, security, and scalability.

## Infrastructure Components

### 1. **Virtual Private Cloud (VPC)**
The VPC module creates a network environment with:
- A custom **CIDR block**
- **Public and private subnets** across multiple availability zones
- **Routing and networking configurations** for secure communication

### 2. **Security Groups**
The security groups module provisions:
- An **ALB security group** allowing inbound HTTP/HTTPS traffic
- An **ECS tasks security group** that only allows traffic from the ALB

### 3. **Application Load Balancer (ALB)**
- Deploys an **Application Load Balancer** in public subnets
- Configures **listener rules** to route traffic to ECS tasks
- Implements **health checks** to monitor the status of ECS tasks

### 4. **Elastic Container Registry (ECR)**
- Creates an **AWS ECR repository** to store container images
- Used as the image source for ECS tasks

### 5. **Elastic Container Service (ECS) - Fargate**
- Deploys an **ECS cluster** with Fargate tasks
- Connects tasks to the **ALB target group**
- Runs **containerized services** with configurable CPU, memory, and environment variables

## Flow of Traffic
1. **Users send requests** → ALB (Public Subnet)
2. **ALB routes requests** → ECS Service (Private Subnet)
3. **ECS tasks run containers** → Containers pull images from ECR
4. **Containers handle traffic and process requests**

## Configuration
### Variables
| Variable | Description |
|----------|-------------|
| `name` | Project name |
| `cidr` | VPC CIDR block |
| `private_subnets` | List of private subnets |
| `public_subnets` | List of public subnets |
| `availability_zones` | Availability zones for subnets |
| `environment` | Deployment environment (e.g., dev, prod) |
| `container_port` | Port exposed by container |
| `container_cpu` | CPU allocation for ECS tasks |
| `container_memory` | Memory allocation for ECS tasks |
| `service_desired_count` | Number of ECS tasks |
| `health_check_path` | Health check path for ALB |

## Deployment
### Prerequisites
- **Terraform installed** (>= 1.0)
- **AWS CLI configured**
- **IAM permissions** for managing AWS resources

### Steps to Deploy
1. **Clone the repository**
   ```sh
   git clone <repo-url>
   cd platform-infrastructure
   ```
2. **Initialize Terraform**
   ```sh
   terraform init
   ```
3. **Preview infrastructure changes**
   ```sh
   terraform plan
   ```
4. **Deploy infrastructure**
   ```sh
   terraform apply -auto-approve
   ```
5. **Retrieve ALB URL** and test the application.

### Destroying Resources
To remove the infrastructure:
```sh
terraform destroy -auto-approve
```

## Conclusion
This Terraform setup provisions a **fully managed, scalable, and secure AWS environment** for running containerized applications. It leverages best practices for networking, security, and service orchestration on AWS.

---

For improvements or modifications, update the Terraform configurations and reapply changes using `terraform apply`.

