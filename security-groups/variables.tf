variable "name" {
  description = "the name of your stack, e.g. hellomicro"
}

variable "environment" {
  description = "the name of your environment, e.g. dev/staging/dev/staging/prod"
}

variable "vpc_id" {
  description = "The VPC ID"
}

variable "container_port" {
  description = "Ingres and egress port of the container"
}